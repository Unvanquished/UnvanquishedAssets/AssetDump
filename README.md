Unvanquished Asset Dump
=======================

Various (and sometime large) assets that may have not been already stored in [UnvanquishedAssets](https://github.com/UnvanquishedAssets/UnvanquishedAssets) repositories.

This may include models that didn't make it or are yet to implement in game, texture sources (like PSD files) that we may not want to store in usual repositories because of them being too large, etc.

The file naming and hierarchy is work in progress, history can be rewritten without warning. This place exists only because it's better than nothing.

See [UnvanquishedAssets](https://github.com/UnvanquishedAssets/UnvanquishedAssets) for ready-to-use repositories with decent file size for assets being actually shipped in game.

Multiple licenses are involved.